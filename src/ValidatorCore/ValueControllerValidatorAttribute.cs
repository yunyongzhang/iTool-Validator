﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;

namespace iTool.Validator
{
    /// <summary>
    /// 具体的验证特性
    /// </summary>
    public class ValueControllerValidatorAttribute : ControllerValidatorAttribute
    {
        /// <summary>
        /// 方法名称和参数要同Controller一致
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="id"></param>
        public void Get(string a1, int id)
        {
            if (a1 != "jian")
                throw new Exception("ai not is jian");

            if (id > 200)
                throw new Exception("id > 200");
        }

    }
}
