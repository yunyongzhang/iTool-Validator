using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using demo.CustomControllerValidator;
using Microsoft.AspNetCore.Mvc;

namespace demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    // 这里引用
    [ValueControllerValidator]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/200
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return $"id is {id}";
        }

        // GET api/values/100/80
        [HttpGet("{id}/{age}")]
        public ActionResult<string> Get(int id, int age)
        {
            return $"id is {id},age is {age}";
        }

        // POST api/values
        [HttpPost]
        public ActionResult<TestModel> Post(TestModel model)
        {
            return model;
        }


    }
}
