using iTool.Validator;
using System;

namespace demo.CustomControllerValidator
{
    /// <summary>
    /// 具体的验证特性
    /// </summary>
    public class ValueControllerValidatorAttribute : ControllerValidatorAttribute
    {
        /// <summary>
        /// 方法名称和参数要同Controller一致
        /// 验证参数支持任意类型Class
        /// </summary>
        public void Get(int id)
        {
            if (id > 200)
                throw new Exception("id > 200");
        }

        public void Get(int id, int age)
        {
            if (age < 200)
                throw new Exception("age < 200");

            if (id > 200)
                throw new Exception("id > 200");
        }

        public void Post(TestModel model)
        {

            if (model == null)
                throw new Exception("我好像并不了解你");

            model.userName = "不管你是什么我都修改你，怎么着吧";
        }

        //public override object ValidatorError(string error)
        //{
        //    // 默认反馈数据结构
        //    // 这里可以自定义
        //    return new
        //    {
        //        code = 10003,
        //        info = error,
        //        data = string.Empty
        //    };
        //}

    }

    public class TestModel
    {
        public int id { get; set; }
        public string userName { get; set; }
    }
}
